# [TEMPLATE] Install GitLab from Source

I do this often enough that I want a little cheatsheet.

## Before
  - [ ] Install the OS on the VM
  - [ ] Take a snapshot of the VM

## During
[Install GitLab from source](https://docs.gitlab.com/ee/install/installation.html#overview)

  - [ ] Get the install instructions for the specific branch you plan to checkout, [start from master](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/install/installation.md) and use the drop-down menu.

Considering taking snapshots in between some of the steps below. You don't have to do it for every single one but snapshots are free and super useful. 

### Steps from the Overview
  - [ ] Packages and dependencies
  - [ ] Ruby
  - [ ] Go
  - [ ] Node
  - [ ] System users
  - [ ] Database
  - [ ] Redis
  - [ ] GitLab
  - [ ] NGINX 
